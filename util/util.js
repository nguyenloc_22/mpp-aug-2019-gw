/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  const uuidv1 = require('uuid/v1');

  let Util = {
    generateUuid: generateUuid,
  };

  module.exports = Util;

  function generateUuid() {
    return uuidv1();
  }

})();
