/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  let logger = console;

  let inst = {
    trace: trace,
    debug: debug,
    info: info,
    warn: warn,
    error: error
  };

  module.exports = inst;

  function trace(params) {
    logger.info('TRACE', params);
  }

  function debug() {
    logger.info(arguments);
  }

  function info() {
    logger.info(arguments);
  }

  function warn() {
    logger.warn(arguments);
  }

  function error() {
    logger.error(arguments);
  }

})();
