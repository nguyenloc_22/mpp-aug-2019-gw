/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  const SocketIoClient = require('socket.io-client');
  const EventEmitter = require('events').EventEmitter;
  const util = require('util');
  const logger = require('../util/logger');
  const appUtil = require('../util/util');

  const mConst = require('../app.constant');

  const Host = mConst.Remote.Host;
  const GwId = mConst.App.GwId;
  const GwName = mConst.App.GwName;
  const Key = mConst.App.Key;

  const MessageTypes = {
    Auth: 'AUTH',
    Report: 'REPORT',
    ReportReq: 'REPORT-REQ',
    Command: 'COMMAND',
    Ack: 'ACK',
    None: 'SERVER-MESSAGE'
  };

  const Constants = {
    Event: {
      SocketInit: 'socket-init',
      RemoteMessage: 'message'
    },

    MessageTypes: MessageTypes
  };

  let DirectCommandEvent = `command-${mConst.App.GwId}`;
  let DisconnectEvent = 'disconnect';

  function SocketClient() {
    this.name = mConst.App.GwName;
    this.isConnected = false;

    testEventEmitter.call(this);
  }

  util.inherits(SocketClient, EventEmitter);

  SocketClient.prototype.constants = Constants;
  SocketClient.prototype.initialize = initialize;
  SocketClient.prototype.sendMessage = sendMessage;

  function initialize() {

    logger.info('Initialize socket client. Host -> ' + Host);
    this.socket = SocketIoClient(Host, {
        autoConnect: false,
        query: { token: '' }
      }
    );
    this.socket.on('connect', onConnected.bind(this));

    // Global events are bound against socket
    this.socket.on('connect_failed', function(){
      console.log('DDDDDD Connection Failed');
    });

    this.socket.open();
  }

  function onConnected() {
    this.isConnected = true;

    this.socket.on(DirectCommandEvent, handleMessage.bind(this));
    this.socket.on(DisconnectEvent, onDisconnected.bind(this));

    sendIdentifyMessage.call(this);

    logger.info(`[socket-io] connected to server @${Host}`);
  }

  function sendIdentifyMessage() {

    let message = {
      id: appUtil.generateUuid(),
      type: MessageTypes.Auth,
      sender: mConst.App.GwId,
      receiver: -1, // -1 for server
      data: {
        name: GwName,
        gatewayId: GwId,
        key: Key
      }
    };

    this.sendMessage(message, 'gw-auth');

  }

  function onDisconnected() {
    this.isConnected = false;

    this.socket.removeAllListeners(DirectCommandEvent);
    this.socket.removeAllListeners(DisconnectEvent);

    logger.info('[socket-io] disconnected from server!');
  }

  function handleMessage(message) {
    logger.debug('[socket-io] Receive new message', message.type);
    // Do emit event to upper layer
    // This layer shall not be in charge of
    // dispatching message
    this.emit(Constants.Event.RemoteMessage, message);
  }

  /**
   *
   * @param message {object}
   */
  function sendMessage(message, eventName) {
    if(!this.isConnected) {
      logger.warn('[socket-io] [sendMessage] SocketIO is not connected yet. Cannot send message.');
      return false;
    }

    if(!message || typeof message !== 'object') {
      logger.warn('[socket-io] [sendMessage] message must be in type of object.');
      return false;
    }

    eventName = eventName || 'message';
    logger.debug('[socket-io] Sending a status report message');
    this.socket.emit(eventName, message);

  }

  function testEventEmitter() {

    if(!this.isConnected) {
      setTimeout(testEventEmitter.bind(this), 1000);
      return;
    }

    console.log(`raiseInitEvent > this[${this.name}]`);
    this.emit(Constants.Event.SocketInit, {
      name: 'demo message from bob-gw'
    });

  }

  module.exports = SocketClient;

})();
