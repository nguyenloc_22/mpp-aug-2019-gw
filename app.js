/**
 * Created by locnv on Sept 9th 2019
 */

(function() {
  "use strict";

  const SocketIO = require('./socket/app.socketio');

  const SwitchManagement = require('./switch').SwitchManagement;

  const AppConst = require('./app.constant');
  const logger = require('./util/logger');
  const util = require('./util/util');

  const DeviceTypes = AppConst.DeviceType;

  let socketIo,
    switchManagement = new SwitchManagement();

  setTimeout(runApp, 1);

  function runApp() {

    socketIo = new SocketIO();
    let Event = socketIo.constants.Event;

    socketIo.on(Event.SocketInit, onSocketIoReady);

    // Listen for Remote events
    socketIo.on(Event.RemoteMessage, dispatchRemoteMessage);
    socketIo.initialize();

    let SwitchEvent = switchManagement.constants.Event;
    switchManagement.on(SwitchEvent.SwitchStatusChanged, onSwitchStatusChanged);

  }

  function onSwitchStatusChanged(sw) {
    sendDevicesStatusReport([sw], []);
  }

  function onSocketIoReady(ctx) {
    logger.debug('[app] Socket Initialized > ', ctx);
    let allSwitches = switchManagement.getAllSwitches();
    sendDevicesStatusReport(allSwitches);
  }

  function sendDevicesStatusReport(switches) {

    logger.info('Sending switches info...');

    let MessageTypes = socketIo.constants.MessageTypes;

    for(let i = 0; i < switches.length; i++) {
      let sw = switches[i];
      let swInfo = toSwitchInfo(sw);
      let message = {
        id: util.generateUuid(),
        type: MessageTypes.Report,
        sender: AppConst.App.GwId,
        receiver: '*',
        data: {
          name: AppConst.App.GwName,
          gatewayId: AppConst.App.GwId,
          deviceId: swInfo.deviceId,
          state: swInfo.state
        }
      };

      socketIo.sendMessage(message);
    }

  }

  function toSwitchInfo(sw) {
    return {
      name: sw.name,
      deviceId: sw.deviceId,
      //devType: DeviceTypes.Switch,
      state: (sw.status === 1) // 1 -> HI / 0 -> LO
    }
  }

  /**
   * Dispatch remote message:
   * - switch command
   * - ...
   * @param message
   */
  function dispatchRemoteMessage(message) {

    //logger.debug('[app] dispatching message', message);
    let messageType = message.type;
    let msgData = message.data;

    let MessageTypes = socketIo.constants.MessageTypes;

    switch(messageType) {

      case MessageTypes.Command:
        if(isSwitchCommand(message)) {
          switchManagement.handleRemoteMessage(msgData);
        } else {
          logger.warn(`[app] receive a direct-command with invalid message type: ${msgData.type}`);
        }

        break;

      case MessageTypes.ReportReq:
        let allSwitches = switchManagement.getAllSwitches();
        sendDevicesStatusReport(allSwitches);
        break;

      default:
        logger.info('[app] un-handled message', msgData);
        break;

    }

  }

  function isSwitchCommand(remoteMessage) {

    return remoteMessage.type === 'COMMAND';
  }

})();
