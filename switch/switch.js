/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  const logger = require('../util/logger');

  const ModuleString = {
    'linux': 'pigpio',
    'darwin': '../mock/pigpio'
  };
  const Platform = process.platform;
  logger.debug('loading switch on ' + Platform);

  const Gpio = require(ModuleString[Platform]).Gpio;

  /* Low / High */
  const LOW = 0, HI = 1;

  const Status = {
    On: LOW, Off: HI,
  };

  const Commands = {
    TurnOn: 'turn-on',
    TurnOff: 'turn-off'
  };

  /**
   *
   * @param config {object}
   *  - name: {string}
   *  - pin: {number} io pin id
   *
   * @constructor
   */
  function Switch(config) {
    this.config = config;
    this.name = config.name || 'unnamed';
    this.deviceId = config.id;
    this.status = Status.Off;
    this.io = new Gpio(config.pin, {mode: Gpio.OUTPUT});

    logger.info(`[switch] ${this.name} on pin#${config.pin}`);
  }

  Switch.constant = {
    Status: Status,
    Commands: Commands
  };

  Switch.prototype.initialize = initialize;
  Switch.prototype.turnOn = turnOn;
  Switch.prototype.turnOff = turnOff;
  Switch.prototype.updateStatus = updateStatus;

  function initialize() {
    this.updateStatus();
  }

  function turnOn() {
    logger.info(`[switch][${this.name}] Turn On.`);
    this.io.digitalWrite(Status.On);
    this.reqStatus = Status.On;
    this.updateStatus();
  }

  function turnOff() {
    logger.info(`[switch][${this.name}] Turn Off.`);
    this.io.digitalWrite(Status.Off);
    this.reqStatus = Status.Off;

    this.updateStatus();
  }

  function updateStatus() {
    function Fn() {
      let level = this.io.digitalRead();
      //logger.info(`[switch][${this.name}] Current level: ${level}.`);

      if(level === Status.On || level === Status.Off) {
        this.status = level;
      }
    }

    setTimeout(Fn.bind(this), 100);
  }

  module.exports = Switch;

})();
