/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  const EventEmitter = require('events').EventEmitter;
  const logger = require('../util/logger');
  const Switch = require('./switch');
  const util = require('util');

  const Constants = {
    Event: {
      SwitchStatusChanged: 'switch-status-changed'
    }
  };

  /* Constructor */
  function SwitchManagement() {
    this.initialize();
    this.tmrUpdateStatus = null;
  }

  util.inherits(SwitchManagement, EventEmitter);

  SwitchManagement.prototype.constants = Constants;
  SwitchManagement.prototype.initialize = initialize;
  SwitchManagement.prototype.getAllSwitches = getAllSwitches;
  SwitchManagement.prototype.handleRemoteMessage = handleRemoteMessage;
  SwitchManagement.prototype.setSwitchStatus = setSwitchStatus;

  /* Initialize */
  function initialize() {
    logger.info('[switch-mng] Initialized');

    this.switches = [ ];
    let DefOfSwitches = require('./definition.json');
    for(let i = 0; i < DefOfSwitches.length; i++) {
      let d = DefOfSwitches[i];
      let sw = new Switch(d);
      sw.initialize();
      this.switches.push(sw);
    }

  }

  function reloadAllSwitchStatus() {

    if(this.tmrUpdateStatus !== null) {
      clearTimeout(this.tmrUpdateStatus);
    }

    let isAllUpdated = true;
    let hasStatusChanged = false;

    for(let i = 0; i < this.switches.length; i++) {
      let sw = this.switches[i];

      if(sw.reqStatus === null) {
        continue;
      }

      if(sw.reqStatus === sw.status) {
        hasStatusChanged = true;
        sw.reqStatus = null;
        this.emit(Constants.Event.SwitchStatusChanged, sw);
      } else {
        sw.updateStatus();
        isAllUpdated = false;
      }
    }

    if(isAllUpdated) {
      return;
    }

    this.tmrUpdateStatus = setTimeout(reloadAllSwitchStatus.bind(this), 100);

  }

  /**
   * Retrieve all available switches
   *
   * @return {properties.switches|{enum}|Array|[*,*]|*}
   */
  function getAllSwitches() {
    return this.switches;
  }

  function handleRemoteMessage(message) {

    let Status = Switch.constant.Status;
    let SwitchCommands = Switch.constant.Commands;

    let switchId = message.deviceId;
    let command = message.state ? SwitchCommands.TurnOff : SwitchCommands.TurnOn;


    switch(command) {

      case SwitchCommands.TurnOn:
        this.setSwitchStatus(switchId, Status.On);
        break;

      case SwitchCommands.TurnOff:
        this.setSwitchStatus(switchId, Status.Off);
        break;

      default:
        logger.warn(`[switch-mng] Invalid command: ${command}`);
        break;
    }

  }

  function setSwitchStatus(switchId, status) {

    let s = findSwitch.call(this, switchId);
    if(!s) {
      logger.warn(`[switch-mng] setSwitchStatus> Switch Not Found [${switchId}]`);
      return false;
    }

    let Status = Switch.constant.Status;
    if(status === Status.On) {
      s.turnOn();
    } else if(status === Status.Off) {
      s.turnOff();
    } else { // Invalid status
      logger.warn(`[switch-mng] Fail to set switch status. Invalid status (${status})`);
      return false;
    }

    reloadAllSwitchStatus.call(this);

    return true;

  }

  function findSwitch(switchId) {
    let found = null;
    let switches = this.switches;

    for(let i = 0; i < switches.length; i++) {
      let s = switches[i];
      if(s.deviceId === switchId) {
        found = s;
        break;
      }
    }

    return found;
  }

  module.exports = SwitchManagement;

})();
