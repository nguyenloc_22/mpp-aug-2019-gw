/**
 * Created by locnv on Sept 9th 2019
 */

(function() {
  "use strict";

  module.exports.Switch = require('./switch');
  module.exports.SwitchManagement = require('./switch.mng');

})();
