/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  function Gpio(pinNb, config) {
    this.pin = pinNb;
    this.config = config;
    this.level = 0;

    console.log(`[gpio][constructor] pin[${this.pin}]`);
  }

  Gpio.prototype.digitalWrite = digitalWrite;
  Gpio.prototype.digitalRead = digitalRead;

  function digitalWrite(level) {
    console.log(`[digitalWrite] pin[${this.pin}].req > ${level}`);

    setTimeout(applyNewValue.bind(this, level), 10);

  }

  function applyNewValue(level) {
    this.level = level;
  }

  function digitalRead() {
    return this.level;
  }

  module.exports = {
    Gpio: Gpio
  };

})();
