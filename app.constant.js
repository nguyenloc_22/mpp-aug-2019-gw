/**
 * Created by locnv on Sept 9th 2019
 */
(function() {
  "use strict";

  const mConst = {
    App: {
      Version: '0.0',
      GwId: 'gw-bob-001',
      GwName: 'Bob-001',
      //Identifier: 'bob-0001',
      Key: 'bob-dev-0001-setp-16-2019'
    },

    Remote: {
      // Host: 'http://localhost:9093'
      Host: 'http://10.10.63.75:9093'
    },

    DeviceType: {
      Switch: 'switch',

      // Other devices such as: Led, Motor, ...
    }

  };

  module.exports = mConst;

})();
